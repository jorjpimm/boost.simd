//==================================================================================================
/*!
  @file

  @copyright 2015 LRI UMR 8623 CNRS/Univ Paris Sud XI
  @copyright 2015 NumScale SAS

  Distributed under the Boost Software License, Version 1.0.
  (See accompanying file LICENSE.md or copy at http://boost.org/LICENSE_1_0.txt)

**/
//==================================================================================================
#ifndef BOOST_SIMD_AS_HPP_INCLUDED
#define BOOST_SIMD_AS_HPP_INCLUDED

#include <boost/simd/config.hpp>
#include <boost/dispatch/as.hpp>

namespace boost { namespace simd
{
  using boost::dispatch::as_;
  using boost::dispatch::as;
} }

#endif
