//==================================================================================================
/*!
  @file

  @copyright 2015 NumScale SAS
  @copyright 2015 J.T.Lapreste

  Distributed under the Boost Software License, Version 1.0.
  (See accompanying file LICENSE.md or copy at http://boost.org/LICENSE_1_0.txt)
*/
//==================================================================================================
#ifndef BOOST_SIMD_FUNCTION_IF_ALLBITS_ELSE_HPP_INCLUDED
#define BOOST_SIMD_FUNCTION_IF_ALLBITS_ELSE_HPP_INCLUDED

#if defined(DOXYGEN_ONLY)
namespace boost { namespace simd
{
  /*!

    @ingroup group-boolean

    If cond is @ref True returns allbits else returns f

    @par Semantic:

    For every parameters of types respectively C, T:

    @code
    T r = if_allbits_else(cond,f);
    @endcode

    is similar to:

    @code
    T r = cond ? Allbits : f;
    @endcode

    @par Alias:
    @c if_nan_else,

    @see genmask, Allbits, Nan


    @return a value of the same type as the second parameter

**/
  template<typename C, typename T> auto if_allbits_else(C const& cond, T const & f) {}

  namespace functional
  {
    /*!
      @ingroup group-callable-boolean

      If cond is @ref True returns allbits else returns f


      Function object tied to simd::if_allbits_else

      @see simd::if_allbits_else
    **/
    const boost::dispatch::functor<tag::if_allbits_else_> if_allbits_else = {};
  }
} }
#endif

#include <boost/simd/function/definition/if_allbits_else.hpp>
#include <boost/simd/arch/common/scalar/function/if_allbits_else.hpp>
#include <boost/simd/function/simd/if_allbits_else.hpp>

#endif
