//==================================================================================================
/*!
  @file

  Copyright 2015 LRI UMR 8623 CNRS/Univ Paris Sud XI
  Copyright 2015 NumScale SAS
  Copyright 2015 J.T.Lapreste

  Distributed under the Boost Software License, Version 1.0.
  (See accompanying file LICENSE.md or copy at http://boost.org/LICENSE_1_0.txt)
*/
//==================================================================================================
#ifndef BOOST_SIMD_FUNCTION_SIMD_DIVROUND_INCLUDED
#define BOOST_SIMD_FUNCTION_SIMD_DIVROUND_INCLUDED

#include <boost/simd/function/scalar/divround.hpp>
//#include <boost/simd/arch/common/function/simd/divround.hpp>

#endif

