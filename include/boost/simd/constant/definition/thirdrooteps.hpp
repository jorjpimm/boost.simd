//==================================================================================================
/*!
  @file

  @copyright 2015 LRI UMR 8623 CNRS/Univ Paris Sud XI
  @copyright 2015 NumScale SAS

  Distributed under the Boost Software License, Version 1.0.
  (See accompanying file LICENSE.md or copy at http://boost.org/LICENSE_1_0.txt)
*/
//==================================================================================================
#ifndef BOOST_SIMD_CONSTANT_DEFINITION_THIRDROOTEPS_HPP_INCLUDED
#define BOOST_SIMD_CONSTANT_DEFINITION_THIRDROOTEPS_HPP_INCLUDED

#include <boost/simd/config.hpp>
#include <boost/simd/detail/brigand.hpp>
#include <boost/simd/detail/dispatch.hpp>
#include <boost/simd/detail/constant_traits.hpp>
#include <boost/dispatch/function/make_callable.hpp>
#include <boost/dispatch/hierarchy/functions.hpp>
#include <boost/dispatch/as.hpp>

namespace boost { namespace simd
{
  namespace tag
  {
    struct thirdrooteps_ : boost::dispatch::constant_value_<thirdrooteps_>
    {
      BOOST_DISPATCH_MAKE_CALLABLE(ext,thirdrooteps_,boost::dispatch::constant_value_<thirdrooteps_>);
      BOOST_SIMD_REGISTER_CONSTANT( 1, 0x3BA14518UL, 0X3ED965FEA53D6E3DULL);
    };
  }

  namespace ext
  {
    BOOST_DISPATCH_FUNCTION_DECLARATION(tag,thirdrooteps_);
  }

  namespace functional
  {
    BOOST_DISPATCH_CALLABLE_DEFINITION(tag::thirdrooteps_,thirdrooteps);
  }

  template<typename T> BOOST_FORCEINLINE auto Thirdrooteps() BOOST_NOEXCEPT
  -> decltype(functional::thirdrooteps( boost::dispatch::as_<T>{}))
  {
    return functional::thirdrooteps( boost::dispatch::as_<T>{} );
  }
} }

#endif
