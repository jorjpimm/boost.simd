//==================================================================================================
/*!
  @file

  Aggregate SIMD numerical and type limits for PPC

  @copyright 2015 LRI UMR 8623 CNRS/Univ Paris Sud XI
  @copyright 2015 NumScale SAS

  Distributed under the Boost Software License, Version 1.0.
  (See accompanying file LICENSE.md or copy at http://boost.org/LICENSE_1_0.txt)

**/
//==================================================================================================
#ifndef BOOST_SIMD_ARCH_POWER_LIMITS_HPP_INCLUDED
#define BOOST_SIMD_ARCH_POWER_LIMITS_HPP_INCLUDED

#include <boost/simd/arch/power/qpx/limits.hpp>
#include <boost/simd/arch/power/vsx/limits.hpp>
#include <boost/simd/arch/power/vmx/limits.hpp>

#endif
