# boost.simd

[![Build Status](https://travis-ci.org/jfalcou/boost.simd.svg)](https://travis-ci.org/jfalcou/boost.simd)

Portable SIMD computation library - To be proposed as a Boost library
